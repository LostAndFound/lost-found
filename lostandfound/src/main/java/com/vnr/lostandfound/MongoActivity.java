package com.vnr.lostandfound;

import java.net.UnknownHostException;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.vnr.lostandfound.manager.MongoDBManager;

public class MongoActivity extends FragmentActivity {

    private MongoDBManager mongoDBManager = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new Thread(new Runnable() {
            public void run() {
                try {
                    mongoDBManager = new MongoDBManager();
                } catch (UnknownHostException e) {
                    // Log.e(TAG, e.getMessage());
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public boolean checkNetworkAvailability() {
        boolean isNetworkAvailable = isNetworkAvailable();
        if (!isNetworkAvailable) {
            AlertDialog.Builder noNetworkDialog = new AlertDialog.Builder(this);

            noNetworkDialog.setTitle(R.string.no_network_title);
            noNetworkDialog.setMessage(R.string.no_network_message);
            noNetworkDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            noNetworkDialog.show();
        }
        return isNetworkAvailable;
    }

    public boolean isNetworkAvailable(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public MongoDBManager getMongoDBManager() {
        if (mongoDBManager == null) {
            try {
                mongoDBManager = new MongoDBManager();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        return mongoDBManager;
    }
}
