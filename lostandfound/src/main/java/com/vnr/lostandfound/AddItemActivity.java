package com.vnr.lostandfound;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.HOUR_OF_DAY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.mongodb.WriteResult;
import com.vnr.lostandfound.domain.Category;
import com.vnr.lostandfound.domain.Item;
import com.vnr.lostandfound.utils.FilesUtils;

public class AddItemActivity extends MongoActivity implements GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    private static final String TAG = "AddItemActivity";

    private static final String PARCELABLE_PIC_URI = "pic_uri";

    private static final int DATE_DIALOG_ID = 0;
    private static final int TIME_DIALOG_ID = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

    private Uri picUri = null;

    public static final String MSG_EXTRA = "msg";
    public static final String ITEM_ID_EXTRA = "item_id";
    public static final String ITEM_TITLE_EXTRA = "item_title";

    public static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy");

    private EditText titleText;
    private Spinner categoriesSpinner;
    private EditText descriptionText;
    private Button setDateButton;
    private Button setTimeButton;
    private ImageView imgPreview;
    private TextView locationAddressText;
    private ProgressBar getAddressIndicator;

    private boolean found = true;

    private Calendar dateTime;

    private LocationClient locationClient = null;
    private Location currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        locationClient = new LocationClient(this, this, this);

        if (getIntent().getExtras() != null) {
            found = getIntent().getExtras().getBoolean("found", true);
        }

        super.onCreate(savedInstanceState);
        if (found) {
            setContentView(R.layout.activity_add_found_item);
            getAddressIndicator = (ProgressBar) findViewById(R.id.address_progress);
            locationAddressText = (TextView) findViewById(R.id.locationAddress);
        } else {
            setTitle(R.string.add_lost_item_title);
            setContentView(R.layout.activity_add_lost_item);
        }

        titleText = (EditText) findViewById(R.id.title);
        categoriesSpinner = (Spinner) findViewById(R.id.category_spinner);
        descriptionText = (EditText) findViewById(R.id.description);
        setDateButton = (Button) findViewById(R.id.setDateButton);
        setTimeButton = (Button) findViewById(R.id.setTimeButton);
        imgPreview = (ImageView) findViewById(R.id.imgPreview);

        initCategories();
        initDateTime();
    }

    private void initCategories() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.categories_array,
                android.R.layout.simple_spinner_item); // simple_spinner_dropdown_item
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categoriesSpinner.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        locationClient.connect();
    }

    private void initDateTime() {
        dateTime = Calendar.getInstance();

        setDateButton.setText(DATE_FORMAT.format(dateTime.getTime()));
        setTimeButton.setText(TIME_FORMAT.format(dateTime.getTime()));
    }

    public void openDateDialog(View view) {
        showDialog(DATE_DIALOG_ID);
    }

    public void openTimeDialog(View view) {
        showDialog(TIME_DIALOG_ID);
    }

    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_DIALOG_ID:
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, dateSetListener, dateTime.get(YEAR),
                    dateTime.get(MONTH), dateTime.get(DAY_OF_MONTH));
            DatePicker datePicker = datePickerDialog.getDatePicker();

            Calendar today = Calendar.getInstance();
            datePicker.setMaxDate(today.getTimeInMillis());

            today.add(MONTH, -3);
            datePicker.setMinDate(today.getTimeInMillis());

            return datePickerDialog;
        case TIME_DIALOG_ID:
            return new TimePickerDialog(this, timeSetListener, dateTime.get(HOUR_OF_DAY), dateTime.get(MINUTE), true);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateTime.set(year, monthOfYear, dayOfMonth);
            setDateButton.setText(DATE_FORMAT.format(dateTime.getTime()));
        }

    };

    private TimePickerDialog.OnTimeSetListener timeSetListener = new TimePickerDialog.OnTimeSetListener() {

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            dateTime.set(HOUR_OF_DAY, hourOfDay);
            dateTime.set(MINUTE, minute);
            setTimeButton.setText(TIME_FORMAT.format(dateTime.getTime()));
        }

    };

    public void captureImage(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        picUri = getOutputFileUri();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, picUri);

        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }

    /**
     * Receiving activity result method will be called after closing the camera
     * */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // if the result is capturing Image
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // successfully captured the image, display it in image view
                previewCapturedImage();
                Toast.makeText(getApplicationContext(), "Image capture succeeded", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                picUri = null;
                Toast.makeText(getApplicationContext(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
            } else {
                picUri = null;
                Toast.makeText(getApplicationContext(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            // If the result code is Activity.RESULT_OK, try to connect again
            switch (resultCode) {
            case Activity.RESULT_OK:
                // TODO: Try the request again
                break;
            }
        }
    }

    private void previewCapturedImage() {
        try {
            imgPreview.setVisibility(View.VISIBLE);

            BitmapFactory.Options options = new BitmapFactory.Options();

            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = ItemInfoActivity.SCALE_SIZE;

            final Bitmap bitmap = BitmapFactory.decodeFile(picUri.getPath(), options);
            imgPreview.setImageBitmap(bitmap);

            imgPreview.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(picUri, "image/*");
                    startActivity(intent);
                }
            });
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public Uri getOutputFileUri() {
        return Uri.fromFile(FilesUtils.getOutputFile());
    }

    public void addItem(View view) {
        boolean isNetworkAvailable = checkNetworkAvailability();
        if (!isNetworkAvailable) {
            return;
        }

        if (found) {
            if (locationAddressText.getVisibility() != View.VISIBLE) {
                Toast.makeText(getApplicationContext(), "Please wait till address is found", Toast.LENGTH_LONG).show();
            }
        }

        final String title = titleText.getText().toString().trim();
        final String description = descriptionText.getText().toString().trim();

        if (title.length() != 0 && description.length() != 0) {
            final Item item = new Item(title, description, found, dateTime.getTime());

            Category category = Category.getCategoryByPos(categoriesSpinner.getSelectedItemPosition());
            item.setCategory(category);

            final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            new Thread(new Runnable() {
                public void run() {
                    Log.i(TAG, "inserting item into database: " + title + ", " + description + ", found? " + found);
                    Resources res = getResources();

                    if (picUri != null) {
                        addPictureToItem(item, picUri);
                    }
                    if (found) {
                        addLocationInfoToItem(item);
                    }

                    WriteResult result = getMongoDBManager().addItem(item);
                    if (result.getError() == null) {
                        setResult(RESULT_OK, intent);
                        intent.putExtra(MSG_EXTRA, res.getString(R.string.item_added_to_database) + title);
                        intent.putExtra(ITEM_ID_EXTRA, item.getItemId());
                        intent.putExtra(ITEM_TITLE_EXTRA, item.getTitle());
                        FilesUtils.deleteFile(picUri);
                    } else {
                        String errorMsg = String.format(res.getString(R.string.failed_to_add_item), title,
                                result.toString());
                        intent.putExtra(MSG_EXTRA, errorMsg);
                        setResult(RESULT_CANCELED, intent);
                        Log.e(TAG, errorMsg);
                    }
                    finish();
                }
            }).start();

        } else {
            AlertDialog.Builder warningDialog = new AlertDialog.Builder(this);

            warningDialog.setMessage(R.string.add_item_warning);
            warningDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            warningDialog.show();
        }
    }

    private void addLocationInfoToItem(Item item) {
        if (servicesConnected() && currentLocation != null) {
            item.setLongitude(currentLocation.getLongitude());
            item.setLatitude(currentLocation.getLatitude());
            String address = locationAddressText.getText().toString().trim();
            if (address.length() > 0) {
                item.setAddress(address);
            }
        } else {
            Toast.makeText(getApplicationContext(), "Location info is not available", Toast.LENGTH_SHORT).show();
        }
    }

    private void getAddress() {
        // Ensure that a Geocoder services is available
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD && Geocoder.isPresent()) {
            // Show the activity indicator
            getAddressIndicator.setVisibility(View.VISIBLE);
            /*
             * Reverse geocoding is long-running and synchronous. Run it on a
             * background thread. Pass the current location to the background
             * task. When the task finishes, onPostExecute() displays the
             * address.
             */
            (new GetAddressTask(this)).execute(currentLocation);
        }

    }

    private void addPictureToItem(Item item, Uri fileUri) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        FileInputStream fis;
        try {
            fis = new FileInputStream(new File(fileUri.getPath()));
            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = fis.read(buf)))
                baos.write(buf, 0, n);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] bbytes = baos.toByteArray();
        item.setPicture(bbytes);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save pic url in bundle as it will be null on screen rotation
        outState.putParcelable(PARCELABLE_PIC_URI, picUri);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        picUri = savedInstanceState.getParcelable(PARCELABLE_PIC_URI);
    }

    public static class ErrorDialogFragment extends DialogFragment {

        private Dialog mDialog;

        public ErrorDialogFragment() {
            super();
            mDialog = null;
        }

        public void setDialog(Dialog dialog) {
            mDialog = dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return mDialog;
        }
    }

    private boolean servicesConnected() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == resultCode) {
            Log.d("Location Updates", "Google Play services is available.");
            return true;
        } else {
            // Get the error code
            // int errorCode = connectionResult.getErrorCode();
            // Get the error dialog from Google Play services
            Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                    CONNECTION_FAILURE_RESOLUTION_REQUEST);

            if (errorDialog != null) {
                ErrorDialogFragment errorFragment = new ErrorDialogFragment();
                errorFragment.setDialog(errorDialog);
                errorFragment.show(getFragmentManager(), "Location Updates");
            }
            return false;
        }
    }

    private class GetAddressTask extends AsyncTask<Location, Void, String> {
        Context mContext;

        public GetAddressTask(Context context) {
            super();
            mContext = context;
        }

        /**
         * Get a Geocoder instance, get the latitude and longitude look up the
         * address, and return it
         * 
         * @params params One or more Location objects
         * @return A string containing the address of the current location, or
         *         an empty string if no address can be found, or an error
         *         message
         */
        @Override
        protected String doInBackground(Location... params) {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            // Get the current location from the input parameter list
            Location loc = params[0];

            if (loc == null) {
                return getResources().getString(R.string.no_location_found);
            }

            // Create a list to contain the result address
            List<Address> addresses = null;
            try {
                /*
                 * Return 1 address.
                 */
                addresses = geocoder.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
            } catch (IOException e1) {
                Log.e("LocationSampleActivity", "IO Exception in getFromLocation()");
                e1.printStackTrace();
                return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " + Double.toString(loc.getLatitude()) + " , "
                        + Double.toString(loc.getLongitude()) + " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                return errorString;
            }
            // If the reverse geocode returned an address
            if (addresses != null && addresses.size() > 0) {
                // Get the first address
                Address address = addresses.get(0);
                /*
                 * Format the first line of address (if available), city, and
                 * country name.
                 */
                String addressText = String.format("%s, %s, %s",
                // If there's a street address, add it
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        // Locality is usually a city
                        address.getLocality(),
                        // The country of the address
                        address.getCountryName());
                // Return the text
                return addressText;
            } else {
                return getResources().getString(R.string.no_address_found);
            }
        }

        /**
         * A method that's called once doInBackground() completes. Turn off the
         * indeterminate activity indicator and set the text of the UI element
         * that shows the address. If the lookup failed, display the error
         * message.
         */
        @Override
        protected void onPostExecute(String address) {
            // Set activity indicator visibility to "gone"
            getAddressIndicator.setVisibility(View.GONE);
            // Display the results of the lookup.
            locationAddressText.setText(address);
            locationAddressText.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onStop() {
        locationClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        FilesUtils.cleanPicDir();
        super.onDestroy();
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
                /*
                 * Thrown if Google Play services canceled the original
                 * PendingIntent
                 */
            } catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        } else {
            /*
             * If no resolution is available, display a dialog to the user with
             * the error.
             */
            // showErrorDialog(connectionResult.getErrorCode()); TODO
        }
    }

    public void onConnected(Bundle arg0) {
        // locationClient.setMockMode(true);
        // locationClient.setMockLocation(getMockLocation());
        if (found) {
            currentLocation = locationClient.getLastLocation();
            // currentLocation = getMockLocation();
            getAddress();
        }
    }

    private Location getMockLocation() {
        Location newLocation = new Location("mock");
        newLocation.setLatitude(37.377166);
        newLocation.setLongitude(-122.086966);
        newLocation.setAccuracy(3.0f);
        return newLocation;
    }

    public void onDisconnected() {
        // TODO Auto-generated method stub
    }

}
