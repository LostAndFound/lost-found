package com.vnr.lostandfound.fragments;

import java.util.HashSet;
import java.util.List;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.vnr.lostandfound.AddItemActivity;
import com.vnr.lostandfound.ItemInfoActivity;
import com.vnr.lostandfound.MainActivity;
import com.vnr.lostandfound.R;
import com.vnr.lostandfound.adapters.ItemsAdapter;
import com.vnr.lostandfound.domain.Item;

public class ItemFragment extends Fragment implements OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = "ItemFragment";

    public static final String ARG_FOUND = "found";

    public static final String EXTRA_ITEM_ID = "item_id";

    private static final int ADD_ITEM_CODE = 0;

    private ItemsAdapter adapter;
    private View rootView;
    private FragmentActivity fa;
    private Button addItemButton;

    private boolean found = true;

    boolean isNetworkAvailable = false;
    private IntentFilter intentFilter;
    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (getArguments() != null) {
            found = getArguments().getBoolean(ARG_FOUND, true);
        }

        fa = super.getActivity();
        rootView = inflater.inflate(R.layout.items_fragment, container, false);

        addItemButton = (Button) rootView.findViewById(R.id.addItemButton);
        addItemButton.setOnClickListener(this);
        if (!found) {
            addItemButton.setText(R.string.add_lost_item);
        }

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new ItemsAdapter(getActivity());

        ListView listView = (ListView) rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (isNetworkAvailable) {
            Intent intent = new Intent(getActivity(), ItemInfoActivity.class);
            String itemIdStr = adapter.getItemIdStr(position);
            intent.putExtra(EXTRA_ITEM_ID, itemIdStr);
            startActivity(intent);
        }
    }

    public void onNetworkConnect() {
        addItemButton.setEnabled(true);
    }

    public void onNetworkDisconnect() {
        addItemButton.setEnabled(false);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart ItemFragment");
        super.onStart();

        isNetworkAvailable = ((MainActivity) getActivity()).isNetworkAvailable();

        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                boolean haveNetworkConnection = ((MainActivity) getActivity()).isNetworkAvailable(context);
                Log.i(TAG, "onReceive ItemFragment: " + haveNetworkConnection);
                isNetworkAvailable = haveNetworkConnection;
                if (isNetworkAvailable) {
                    onNetworkConnect();
                } else {
                    onNetworkDisconnect();
                }
            }
        };

        intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        getActivity().registerReceiver(broadcastReceiver, intentFilter);

        if (!isNetworkAvailable) {
            onNetworkDisconnect();
            return;
        }

        if (adapter.getCount() == 0) {
            loadItems();
        }
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume ItemFragment");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause ItemFragment");
        getActivity().unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    private void loadItems() {
        Log.i(TAG, "loadItems " + found + ", ItemFragment");
        new Thread(new Runnable() {
            public void run() {
                // for list of items load from database only titles of items
                // (+ _id comes by default)
                HashSet<String> fields = new HashSet<String>();
                fields.add("title");

                final List<Item> items = ((MainActivity) getActivity()).getMongoDBManager().getItems(found, fields);
                getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        for (Item item : items) {
                            adapter.add(item);
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i(TAG, "onActivityResult");

        if (requestCode == ADD_ITEM_CODE) {
            if (data != null && data.getExtras() != null) {

                String msg = data.getExtras().getString(AddItemActivity.MSG_EXTRA);
                Toast.makeText(fa, msg, Toast.LENGTH_LONG).show();

                if (resultCode == Activity.RESULT_OK) {
                    String itemId = data.getExtras().getString(AddItemActivity.ITEM_ID_EXTRA);
                    String title = data.getExtras().getString(AddItemActivity.ITEM_TITLE_EXTRA);

                    adapter.add(new Item(itemId, title));
                }
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.addItemButton:
            Intent intent = new Intent(getActivity(), AddItemActivity.class);
            intent.putExtra(ARG_FOUND, found);
            startActivityForResult(intent, ADD_ITEM_CODE);
            break;
        }
    }
}
