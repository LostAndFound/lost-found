package com.vnr.lostandfound.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.bson.types.ObjectId;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.WriteResult;
import com.vnr.lostandfound.domain.Category;
import com.vnr.lostandfound.domain.Item;

public class ItemDAO {

    private DBCollection itemsCollection;

    public ItemDAO(final DB database) {
        itemsCollection = database.getCollection("items");
    }

    public WriteResult addItem(Item item) {
        return addItem(item, new ObjectId());
    }

    public WriteResult addItem(Item item, ObjectId id) {
        item.setItemId(id.toString());

        BasicDBObject itemDBObj = new BasicDBObject();
        itemDBObj.append("_id", id);
        itemDBObj.append("title", item.getTitle());
        itemDBObj.append("description", item.getDescription());
        itemDBObj.append("found", item.isFound());
        itemDBObj.append("addedOn", item.getAddedOn());
        itemDBObj.append("lostOrFoundOn", item.getLostOrFoundOn());
        itemDBObj.append("latitude", item.getLatitude());
        itemDBObj.append("longitude", item.getLongitude());
        itemDBObj.append("address", item.getAddress());
        itemDBObj.append("category", item.getCategory().getPos());

        if (item.getPicture() != null) {
            // TODO: save to database only thumbnail
            itemDBObj.append("picture", item.getPicture());
        }

        WriteResult insertResult = itemsCollection.insert(itemDBObj);
        return insertResult;
    }

    public List<Item> getItems(boolean found, Set<String> fields) {
        BasicDBObject findObj = new BasicDBObject("found", found);
        BasicDBObject keys = null;
        if (fields != null && !fields.isEmpty()) {
            keys = new BasicDBObject();
            for (String string : fields) {
                keys.put(string, 1);
            }
        }

        DBCursor cursor = (keys == null) ? itemsCollection.find(findObj) : itemsCollection.find(findObj, keys);

        List<Item> items = new ArrayList<Item>();
        try {
            while (cursor.hasNext()) {
                DBObject itemDBObject = cursor.next();
                Item item = createItemFromDBObject(itemDBObject);
                items.add(item);
            }
        } finally {
            cursor.close();
        }
        return items;
    }

    public List<Item> getItems(boolean found) {
        return getItems(found, null);
    }

    private Item createItemFromDBObject(DBObject itemDBObject) {
        Item item = new Item();

        String idStr = itemDBObject.get("_id").toString();
        item.setItemId(idStr);

        String title = (String) itemDBObject.get("title");
        if (title != null) {
            item.setTitle(title);
        }

        String description = (String) itemDBObject.get("description");
        if (description != null) {
            item.setDescription(description);
        }

        Boolean foundBool = (Boolean) itemDBObject.get("found");
        if (foundBool != null) {
            item.setFound(foundBool);
        }

        Date lostOrFoundOn = (Date) itemDBObject.get("lostOrFoundOn");
        if (lostOrFoundOn != null) {
            item.setLostOrFoundOn(lostOrFoundOn);
        }

        Integer category = (Integer) itemDBObject.get("category");
        if (category != null) {
            item.setCategory(Category.getCategoryByPos(category));
        }

        String address = (String) itemDBObject.get("address");
        if (address != null) {
            item.setAddress(address);
        }

        byte[] picture = (byte[]) itemDBObject.get("picture");
        if (picture != null) {
            item.setPicture(picture);
        }

        Date addedOnDate = (Date) itemDBObject.get("addedOn");
        if (addedOnDate != null) {
            item.setAddedOn(addedOnDate);
        }

        return item;
    }

    public List<Item> getFoundItems() {
        return getItems(true);
    }

    public List<Item> getLostItems() {
        return getItems(false);
    }

    public Item getItem(String id) {
        DBObject itemObj = itemsCollection.findOne(new BasicDBObject("_id", new ObjectId(id)));
        if (itemObj == null) {
            return null;
        }

        Item item = createItemFromDBObject(itemObj);
        return item;
    }
}
