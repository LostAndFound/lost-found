package com.vnr.lostandfound.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vnr.lostandfound.fragments.ItemFragment;

public class TabsPagerAdapter extends FragmentPagerAdapter {

    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int index) {
        ItemFragment itemFragment = new ItemFragment();
        switch (index) {
        case 0:
            return itemFragment;
        case 1:
            Bundle args = new Bundle();
            args.putBoolean(ItemFragment.ARG_FOUND, false);
            itemFragment.setArguments(args);
            return itemFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

}
