package com.vnr.lostandfound.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.vnr.lostandfound.R;
import com.vnr.lostandfound.domain.Item;

public class ItemsAdapter extends BaseAdapter {

    private final Context context;

    private final List<Item> items = new ArrayList<Item>();

    public ItemsAdapter(Context context) {
        this.context = context;
    }

    public void add(Item item) {
        items.add(item);
        notifyDataSetChanged();
    }

    public int getCount() {
        return items.size();
    }

    public Item getItem(int position) {
        return items.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public String getItemIdStr(int position) {
        Item item = getItem(position);
        return item.getItemId();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        final Item item = (Item) getItem(position);

        LayoutInflater inflater = LayoutInflater.from(context);
        RelativeLayout itemLayout = (RelativeLayout) inflater.inflate(R.layout.item, null);

        final TextView titleView = (TextView) itemLayout.findViewById(R.id.itemTitle);
        titleView.setText(item.getTitle());

        return itemLayout;
    }

}
