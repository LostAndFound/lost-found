package com.vnr.lostandfound.utils;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.net.Uri;
import android.os.Environment;
import android.util.Log;

public class FilesUtils {

    private static final String TAG = "FilesUtils";

    // directory name to store files for app
    private static final String PIC_DIR_NAME = "lostandfound";

    public static void cleanPicDir() {
        File picDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                PIC_DIR_NAME);
        cleanDir(picDir);
    }

    public static void cleanDir(File dir) {
        if (dir.exists() && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                new File(dir, children[i]).delete();
            }
        }
    }

    public static void deleteFile(Uri uri) {
        if (uri != null) {
            File file = new File(uri.getPath());
            boolean deleted = file.delete();
            if (!deleted) {
                Log.w(TAG, "Failed to delete file " + uri.getPath() + " from sd card.");
            }
        }
    }

    public static File getOutputFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                PIC_DIR_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(PIC_DIR_NAME, "Oops! Failed create " + PIC_DIR_NAME + " directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        File picFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return picFile;
    }

}
