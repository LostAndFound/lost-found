package com.vnr.lostandfound;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vnr.lostandfound.domain.Item;
import com.vnr.lostandfound.fragments.ItemFragment;

public class ItemInfoActivity extends MongoActivity {

    public static final int SCALE_SIZE = 8;

    private LinearLayout loadingLayout;
    private LinearLayout itemInfoLayout;

    private ImageView itemPicture;
    private TextView title;
    private TextView description;
    private TextView category;
    private TextView lostFoundOnLabel;
    private TextView foundOrLostOn;
    private TextView location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.item_info);

        loadingLayout = (LinearLayout) findViewById(R.id.loadingLayout);
        itemInfoLayout = (LinearLayout) findViewById(R.id.itemInfoLayout);

        itemPicture = (ImageView) findViewById(R.id.itemPicture);
        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        category = (TextView) findViewById(R.id.category);
        lostFoundOnLabel = (TextView) findViewById(R.id.lostFoundOnLabel);
        foundOrLostOn = (TextView) findViewById(R.id.lostFoundOn);
        location = (TextView) findViewById(R.id.locationAddress);
    }

    @Override
    protected void onResume() {
        super.onResume();
        boolean checkNetworkAvailability = checkNetworkAvailability();

        if (!checkNetworkAvailability) {
            return;
        }

        Intent intent = getIntent();
        final String itemId = intent.getStringExtra(ItemFragment.EXTRA_ITEM_ID);
        if (itemId != null) {
            new Thread(new Runnable() {
                public void run() {
                    final Item item = getMongoDBManager().getItem(itemId);
                    if (item != null) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                if (!item.isFound()) {
                                    lostFoundOnLabel.setText(R.string.lost_on_label);
                                }

                                title.setText(item.getTitle());
                                description.setText(item.getDescription());
                                category.setText(getResources().getStringArray(R.array.categories_array)[item
                                        .getCategory().getPos()]);
                                foundOrLostOn.setText(AddItemActivity.DATE_FORMAT.format(item.getLostOrFoundOn()));
                                location.setText(item.getAddress());

                                byte[] picture = item.getPicture();
                                if (picture != null) {
                                    BitmapFactory.Options options = new BitmapFactory.Options();
                                    options.inSampleSize = SCALE_SIZE;
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(picture, 0, picture.length, options);
                                    itemPicture.setImageBitmap(bitmap);
                                }

                                loadingLayout.setVisibility(View.GONE);
                                itemInfoLayout.setVisibility(View.VISIBLE);

                            }
                        });
                    }
                }
            }).start();
        }
    }
}
