package com.vnr.lostandfound.manager;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import com.vnr.lostandfound.dao.ItemDAO;
import com.vnr.lostandfound.domain.Item;

public class MongoDBManager {

    private ItemDAO itemDAO = null;

    public MongoDBManager() throws UnknownHostException {
        MongoClient mongoClient = MongoHelper.getMongoClient();

        DB items = mongoClient.getDB(MongoHelper.DB_NAME);
        itemDAO = new ItemDAO(items);
    }

    public Item getItem(String id) {
        Item item = itemDAO.getItem(id);
        return item;
    }

    public List<Item> getItems(boolean found) {
        return getItems(found, null);
    }

    public List<Item> getItems(boolean found, Set<String> fields) {
        List<Item> foundItems = itemDAO.getItems(found, fields);
        return foundItems;
    }

    public WriteResult addItem(Item item) {
        WriteResult result = itemDAO.addItem(item);
        return result;
    }
}
