package com.vnr.lostandfound.manager;

import java.net.UnknownHostException;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

public class MongoHelper {

    private final static String HOST = "ds063287.mongolab.com";
    private final static String PORT = "63287";

    static final String DB_NAME = "lostandfound";

    private final static String USERNAME = "user";
    private final static String PASS = "pass";

    private static MongoClient mongoClient = null;

    private MongoHelper() {
    }

    public static synchronized MongoClient getMongoClient() throws UnknownHostException {
        if (mongoClient == null) {
            MongoClientURI mongoClientURI = new MongoClientURI("mongodb://" + USERNAME + ":" + PASS + "@" + HOST + ":"
                    + PORT + "/" + DB_NAME);
            mongoClient = new MongoClient(mongoClientURI);
        }
        return mongoClient;
    }

}
