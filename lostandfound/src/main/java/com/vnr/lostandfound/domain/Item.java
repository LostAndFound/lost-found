package com.vnr.lostandfound.domain;

import java.util.Date;

public class Item {

    private String itemId;

    private String title;
    private String description;
    private Category category;
    private boolean found;
    private Date addedOn;
    private Date lostOrFoundOn;
    private byte[] picture;

    private double latitude;
    private double longitude;
    private String address;

    public Item() {
    }

    public Item(String id, String title) {
        this.itemId = id;
        this.title = title;
    }

    public Item(String title, String description, Date lostOrFoundOn) {
        this(title, description, true, lostOrFoundOn);
    }

    public Item(String title, String description, boolean found, Date lostOrFoundOn) {
        this.title = title;
        this.description = description;
        this.found = found;
        this.lostOrFoundOn = lostOrFoundOn;
        this.addedOn = new Date();
    }

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean found) {
        this.found = found;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getAddedOn() {
        return addedOn;
    }

    public void setAddedOn(Date addedOn) {
        this.addedOn = addedOn;
    }

    public Date getLostOrFoundOn() {
        return lostOrFoundOn;
    }

    public void setLostOrFoundOn(Date lostOrFoundOn) {
        this.lostOrFoundOn = lostOrFoundOn;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

}
