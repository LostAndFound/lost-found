package com.vnr.lostandfound.domain;

public enum Category {
    ELECTRONICS(0), KEYS(1), DOCUMENTS(2), CLOTHES(3), ACCESSORIES(4);

    private int pos;

    private Category(int position) {
        this.pos = position;
    }

    public int getPos() {
        return pos;
    }

    public static Category getCategoryByPos(int pos) {
        for (Category category : Category.values()) {
            if (category.getPos() == pos) {
                return category;
            }
        }
        return null;
    }
}
